package fr.soat.katasg.impl.sg;

import java.math.BigDecimal;

import fr.soat.katasg.api.Amount;
import fr.soat.katasg.api.InvalidOperationException;
import fr.soat.katasg.api.Operation;

public class CreditOperation extends Operation {

	public CreditOperation(final Amount amount, final Amount oldBalance) throws InvalidOperationException {
		super(amount, oldBalance);
	}

	@Override
	protected Amount calculateNewBalance(final Amount oldBalance, final Amount amount) {

		final BigDecimal newAmountValue = oldBalance.getValue().add(amount.getValue());
		return Amount.of(newAmountValue, amount.getCurrency());
	}

	@Override
	public String getOperationType() {
		return "Credit";
	}
}
