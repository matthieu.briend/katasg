package fr.soat.katasg.impl.sg;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import fr.soat.katasg.api.Operations;

public class OperationsDisplay {

	private static final String FIELD_SEPARATOR = "\t";
	private static final DateTimeFormatter DATE_PATTERN = DateTimeFormatter.ofPattern("yyyy/MM/dd");

	public static String display(final Operations operations) {
		final StringBuilder sb = new StringBuilder("Operation" + FIELD_SEPARATOR + "Date" + FIELD_SEPARATOR + "Amount"
				+ FIELD_SEPARATOR + "Balance" + System.lineSeparator());
		operations.forEach(operation -> //
		sb.append(format(operation.getOperationType())) //
				.append(formatted(operation.getOperationDate())) //
				.append(format(operation.getAmount())) //
				.append(operation.getBalance()) //
				.append(System.lineSeparator()));
		return sb.toString();
	}

	private static String format(final Object operationType) {
		return operationType + FIELD_SEPARATOR;
	}

	private static String formatted(final LocalDate operationDate) {
		return format(operationDate.format(DATE_PATTERN));
	}
}
