package fr.soat.katasg.impl.sg;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fr.soat.katasg.api.Amount;
import fr.soat.katasg.api.BankAccount;
import fr.soat.katasg.api.InvalidOperationException;
import fr.soat.katasg.api.Operation;
import fr.soat.katasg.api.Operations;

public class SGBankAccount implements BankAccount {

	private final List<Operation> operations = new ArrayList<Operation>();

	@Override
	public void credit(final Amount amount) throws InvalidOperationException {
		checkAmountValue(amount);
		final Amount lastBalance = findLastBalance(amount);
		checkAmountCurrency(amount, lastBalance);

		this.operations.add(new CreditOperation(amount, lastBalance));
	}

	private Amount findLastBalance(final Amount amount) {
		if (this.operations.isEmpty()) {
			return Amount.of(BigDecimal.ZERO, amount.getCurrency());
		}
		return this.operations.get(this.operations.size() - 1).getBalance();
	}

	@Override
	public void debit(final Amount amount) throws InvalidOperationException {
		checkAmountValue(amount);
		final Amount lastBalance = findLastBalance(amount);
		checkAmountCurrency(amount, lastBalance);

		final DebitOperation debitOperation = new DebitOperation(amount, lastBalance);
		this.operations.add(debitOperation);
	}

	private void checkAmountValue(final Amount amount) throws InvalidOperationException {
		if (amount.getValue().compareTo(BigDecimal.ZERO) <= 0) {
			throw new InvalidOperationException("Amount must be strictly positive");
		}
	}

	/*
	 * For sake of simplicity, we will consider that all the operations must have
	 * the same currency. Otherwise we would have considered currency conversion.
	 */
	private void checkAmountCurrency(final Amount amount, final Amount lastBalance) throws InvalidOperationException {
		if (!amount.getCurrency().equals(lastBalance.getCurrency())) {
			throw new InvalidOperationException("Operations have different currencies");
		}
	}

	@Override
	public Operations retrieveHistory() {
		return new Operations(Collections.unmodifiableList(this.operations));
	}
}
