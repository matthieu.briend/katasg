package fr.soat.katasg.api;

public class InvalidOperationException extends Exception {

	private static final long serialVersionUID = 1L;

	public InvalidOperationException(final String message) {
		super(message);
	}
}
