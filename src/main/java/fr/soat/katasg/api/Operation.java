package fr.soat.katasg.api;

import java.math.BigDecimal;
import java.time.LocalDate;

public abstract class Operation {

	private final LocalDate operationDate;
	private final Amount amount;
	private final Amount balance;

	public Operation(final Amount amount, final Amount oldBalance) throws InvalidOperationException {
		this.amount = amount;
		this.operationDate = LocalDate.now();
		this.balance = calculateNewBalance(oldBalance, amount);
		if (this.balance.getValue().compareTo(BigDecimal.ZERO) < 0) {
			throw new InvalidOperationException("Not enough money");
		}
	}

	protected abstract Amount calculateNewBalance(Amount oldBalance, Amount amount);

	public abstract String getOperationType();

	public Amount getAmount() {
		return this.amount;
	}

	public Amount getBalance() {
		return this.balance;
	}

	public LocalDate getOperationDate() {
		return this.operationDate;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append(this.operationDate);
		builder.append("\t");
		builder.append(this.amount);
		builder.append("\t");
		builder.append(this.balance);
		return builder.toString();
	}
}
