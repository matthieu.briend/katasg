package fr.soat.katasg.api;

import java.util.List;
import java.util.function.Consumer;

public class Operations {
	private final List<Operation> operations;

	public Operations(final List<Operation> operationList) {
		this.operations = operationList;
	}

	public int number() {
		return this.operations.size();
	}

	public Amount lastBalance() {
		return this.operations.get(this.operations.size() - 1).getBalance();
	}

	public void forEach(final Consumer<? super Operation> action) {
		this.operations.forEach(action);
	}
}
