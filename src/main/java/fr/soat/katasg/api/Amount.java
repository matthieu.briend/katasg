package fr.soat.katasg.api;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Currency;

public class Amount {

	private final BigDecimal value;
	private final Currency currency;

	private Amount(final BigDecimal value, final Currency currency) {
		this.value = value.setScale(2, RoundingMode.HALF_EVEN);
		this.currency = currency;
	}

	public static Amount of(final BigDecimal value, final Currency currency) {
		return new Amount(value, currency);
	}

	public static Amount of(final String value, final String currency) {
		return new Amount(new BigDecimal(value), Currency.getInstance(currency));
	}

	public BigDecimal getValue() {
		return this.value;
	}

	public Currency getCurrency() {
		return this.currency;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.currency == null) ? 0 : this.currency.hashCode());
		result = prime * result + ((this.value == null) ? 0 : this.value.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Amount other = (Amount) obj;
		if (this.currency == null) {
			if (other.currency != null) {
				return false;
			}
		} else if (!this.currency.equals(other.currency)) {
			return false;
		}
		if (this.value == null) {
			if (other.value != null) {
				return false;
			}
		} else if (!this.value.equals(other.value)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return this.value + " " + this.currency;
	}
}
