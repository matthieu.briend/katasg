package fr.soat.katasg.api;

public interface BankAccount {
	void credit(Amount amount) throws InvalidOperationException;

	void debit(Amount amount) throws InvalidOperationException;

	Operations retrieveHistory();
}
