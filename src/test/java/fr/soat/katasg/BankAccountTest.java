package fr.soat.katasg;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Currency;
import java.util.stream.Stream;

import org.assertj.core.api.ThrowableAssert.ThrowingCallable;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import fr.soat.katasg.api.Amount;
import fr.soat.katasg.api.BankAccount;
import fr.soat.katasg.api.InvalidOperationException;
import fr.soat.katasg.api.Operations;
import fr.soat.katasg.impl.sg.OperationsDisplay;
import fr.soat.katasg.impl.sg.SGBankAccount;

@DisplayName("A bank account")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class BankAccountTest {

	@Nested
	@DisplayName("should be credited")
	class ShouldBeCredited {
		@Test
		void when_client_deposits_money_once() throws InvalidOperationException {
			// given
			final BankAccount bankAccount = new SGBankAccount();

			// when
			bankAccount.credit(Amount.of("3.14", "EUR"));

			// then
			final Operations operations = bankAccount.retrieveHistory();
			assertThat(operations.number()).isEqualTo(1);
			assertThat(operations.lastBalance()).isEqualTo(Amount.of("3.14", "EUR"));
		}

		@Test
		void when_client_deposits_money_multiple_times() throws InvalidOperationException {
			// given
			final BankAccount bankAccount = new SGBankAccount();

			// when
			bankAccount.credit(Amount.of("3.14", "EUR"));
			bankAccount.credit(Amount.of("2.72", "EUR"));
			bankAccount.credit(Amount.of("42", "EUR"));

			// then
			final Operations operations = bankAccount.retrieveHistory();
			assertThat(operations.number()).isEqualTo(3);
			assertThat(operations.lastBalance()).isEqualTo(Amount.of("47.86", "EUR"));
		}
	}

	@Test
	void should_retrieve_history_of_operations() throws InvalidOperationException {
		// given
		final BankAccount bankAccount = new SGBankAccount();

		// when
		bankAccount.credit(Amount.of("3.14", "EUR"));
		bankAccount.credit(Amount.of("2.72", "EUR"));
		bankAccount.credit(Amount.of("42", "EUR"));
		bankAccount.debit(Amount.of(BigDecimal.TEN, Currency.getInstance("EUR")));
		bankAccount.credit(Amount.of("5", "EUR"));
		bankAccount.debit(Amount.of("0.01", "EUR"));

		// then
		final Operations operations = bankAccount.retrieveHistory();
		assertThat(operations.number()).isEqualTo(6);
		assertThat(operations.lastBalance()).isEqualTo(Amount.of("42.85", "EUR"));
		assertThat(OperationsDisplay.display(operations)).isEqualTo(//
				"Operation\tDate\tAmount\tBalance\n" + //
						"Credit\t" + today() + "\t3.14 EUR\t3.14 EUR\n" + //
						"Credit\t" + today() + "\t2.72 EUR\t5.86 EUR\n" + //
						"Credit\t" + today() + "\t42.00 EUR\t47.86 EUR\n" + //
						"Debit\t" + today() + "\t10.00 EUR\t37.86 EUR\n" + //
						"Credit\t" + today() + "\t5.00 EUR\t42.86 EUR\n" + //
						"Debit\t" + today() + "\t0.01 EUR\t42.85 EUR\n");
	}

	private String today() {
		final DateTimeFormatter datePattern = DateTimeFormatter.ofPattern("yyyy/MM/dd");
		final LocalDate today = LocalDate.now();
		return today.format(datePattern);
	}

	@Nested
	@DisplayName("should be debited")
	class ShouldBeDebited {

		@Test
		void when_client_retrieves_some_money_but_not_all() throws InvalidOperationException {
			// given
			final BankAccount bankAccount = new SGBankAccount();
			bankAccount.credit(Amount.of("64", "EUR"));

			// when
			bankAccount.debit(Amount.of(BigDecimal.TEN, Currency.getInstance("EUR")));

			// then
			final Operations operations = bankAccount.retrieveHistory();
			assertThat(operations.number()).isEqualTo(2);
			assertThat(operations.lastBalance()).isEqualTo(Amount.of("54", "EUR"));
		}

		@Test
		void when_client_retrieves_all_his_money() throws InvalidOperationException {
			// given
			final BankAccount bankAccount = new SGBankAccount();
			bankAccount.credit(Amount.of("64", "EUR"));

			// when
			bankAccount.debit(Amount.of("64.00", "EUR"));

			// then
			final Operations operations = bankAccount.retrieveHistory();
			assertThat(operations.number()).isEqualTo(2);
			assertThat(operations.lastBalance()).isEqualTo(Amount.of("0.00", "EUR"));
		}
	}

	@Nested
	@DisplayName("should throw an error")
	class ShouldThrowAnError {

		@Test
		void when_client_debits_too_much_money() throws InvalidOperationException {
			// given
			final BankAccount bankAccount = new SGBankAccount();
			bankAccount.credit(Amount.of("64", "EUR"));

			// when
			final Throwable thrown = catchThrowable(() -> {
				bankAccount.debit(Amount.of("128.00", "EUR"));
			});

			// then
			assertThat(thrown).isInstanceOf(InvalidOperationException.class).hasMessage("Not enough money");
		}

		@Test
		void when_client_debits_an_empty_account() {
			// given
			final BankAccount bankAccount = new SGBankAccount();

			// when
			final Throwable thrown = catchThrowable(() -> {
				bankAccount.debit(Amount.of("128.00", "EUR"));
			});

			// then
			assertThat(thrown).isInstanceOf(InvalidOperationException.class).hasMessage("Not enough money");
		}

		@ParameterizedTest(name = "Invalid value: {0} EUR")
		@ValueSource(strings = { "-12", "0" })
		void when_debit_amounts_have_unsupported_value(final String unsupportedValue) {
			// given
			final BankAccount bankAccount = new SGBankAccount();

			// when
			final Throwable thrown = catchThrowable(() -> bankAccount.debit(Amount.of(unsupportedValue, "EUR")));

			// then
			assertThat(thrown).isInstanceOf(InvalidOperationException.class)
					.hasMessage("Amount must be strictly positive");
		}

		@ParameterizedTest(name = "Invalid value: {0} EUR")
		@ValueSource(strings = { "-12", "0" })
		void when_credit_amounts_have_unsupported_value(final String unsupportedValue) {
			// given
			final BankAccount bankAccount = new SGBankAccount();

			// when
			final Throwable thrown = catchThrowable(() -> bankAccount.credit(Amount.of(unsupportedValue, "EUR")));

			// then
			assertThat(thrown).isInstanceOf(InvalidOperationException.class)
					.hasMessage("Amount must be strictly positive");
		}

		@ParameterizedTest(name = "Operations: {1}")
		@MethodSource("fr.soat.katasg.BankAccountTest#operationsWithDifferentCurrencies")
		void when_two_operations_have_different_currencies(
				// given
				// several operations, one having a different currency
				final ThrowingCallable operations, final String operationsDescription) {

			// when
			final Throwable thrown = catchThrowable(operations);

			// then
			assertThat(thrown).isInstanceOf(InvalidOperationException.class)
					.hasMessage("Operations have different currencies");
		}
	}

	@SuppressWarnings("unused") // used in
								// fr.soat.katasg.BankAccountTest.ShouldThrowAnError.when_two_operations_have_different_currencies(ThrowingCallable)
	private static Stream<Arguments> operationsWithDifferentCurrencies() {
		return Stream.of(

				Arguments.of((ThrowingCallable) () -> {
					final BankAccount bankAccount = new SGBankAccount();
					bankAccount.credit(Amount.of("64", "EUR"));
					bankAccount.debit(Amount.of("32", "GBP")); // different currency
				}, "credit(EUR), debit(GBP)"),

				Arguments.of((ThrowingCallable) () -> {
					final BankAccount bankAccount = new SGBankAccount();
					bankAccount.credit(Amount.of("64", "GBP"));
					bankAccount.debit(Amount.of("32", "GBP"));
					bankAccount.credit(Amount.of("64", "EUR")); // different currency
				}, "debit(GBP), credit(EUR)"));
	}
}
